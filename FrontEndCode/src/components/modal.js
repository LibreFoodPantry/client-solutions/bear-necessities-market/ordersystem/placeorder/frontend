/**
 * Filename: modal.js
 * Description: JavaScript for the modal after order has been submitted
 */

//Getting the modal
var modal = document.getElementById("submitModal");

//Getting the button that opens the modal
var button = document.getElementById("submitButton");

//Getting the "span" area to be closed
var span = document.getElementsByClassName("close")[0];

//When the button is clicked
button.onclick = function() {
    modal.style.display = "block";
};

//When user clicks [x], close the modal
span.onclick = function() {
    modal.style.display = "none";
    window.location.reload();
};

//When user clicks anywhere outside the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
        window.location.reload();
    }
};
