# BNM FrontEnd

## Purpose

The PlaceOrder FrontEnd currently utilizes React App in order to provide a dynamic UI that allows users to select items, provide personal preferences and allergies, and ultimately place orders to the BNM food pantry. This is accomplished through utilizing api calls to connect to the PlaceOrder Backend and store the orders within the mongo database.

## Current status

Currently the project runs as a Vue App, and has new interface improvements, such as an error modal on an unsuccessful order and functioning e-mail verification entry fields. The project also now runs under Yarn, an improved package manager from NPM. The project can now be run in a Docker Container using the instructions at the bottom of this page. Installation instructions for Vue can be found at https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/ordersystem/placeorder/frontend/-/blob/main/PlaceOrderFront/README.md

### Next Steps

Looking ahead, the FrontEnd Microservice intends to integrate into the PlaceOrder BackEnd by passing orders through the PlaceOrder API. This is planned to be implemented in the Vue App by utilizing Axios, an HTTP client for node.js and the browser, to pass order items to the other Microservices. Currently this project's Docker Container does NOT work on a Mac OS, and will require changes to the Dockerfile to run on all Operating Systems. This project is planning to get rid of the "FrontEndCode" and "PlaceOrderFront" repositories and place all relevant source files at the top of the repo (Please check Issue #46 if you would like to follow up on this). This project has also considered a mock database to test the functionality of the FrontEnd locally, if interested, please check Issues #58 and #59.

## Learn More

- If you would like to learn more about the Vue framework you can check out their extensive documentation and tutorials here: https://vuejs.org/guide/introduction.html

### LibreFoodPantry Vue Example

- If you would like to check out a simple example of how Vue is being utilized within this specific project check out this link: https://gitlab.com/LibreFoodPantry/training/microservices/microservices-examples/frontend


## Running the FrontEnd in a Docker Container
- **PLEASE NOTE THAT IF YOU RUN THE VUE APP IN A CONTAINER THE HOT-RELOAD DEVELOPMENT FEATURE WILL NOT BE AVAILABLE. IF YOU WOULD LIKE HOT-RELOAD, RUN THE VUE APP USING THE YARN INSTRUCTIONS LOCATED IN THE "PlaceOrderFront" DIRECTORY**

### Clone this project
git clone https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/ordersystem/placeorder/frontend.git

### Access
cd ./frontend

### Run the project
docker-compose up

# The Frontend server will initialize in the <http://localhost:3000>
